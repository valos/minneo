# -*- coding: utf-8 -*-

# Minneo -- A classic Memory game for mobile devices
#
# Copyright (C) 2010-2012 Valéry Febvre <vfebvre@easter-eggs.com>
# http://code.google.com/p/minneo/
#
# This file is part of Minneo.
#
# Minneo is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# Minneo is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import os
from distutils.core import setup

def get_data_files():
    l =  [
        ('share/applications', ['data/minneo.desktop']),
        ('share/pixmaps', ['data/minneo.png']),
        ('share/minneo', ['README']),
        ('share/minneo/sounds', ['data/sounds/yeah.wav']),
        ]
    l += get_cards_themes()
    l += get_gui_themes()
    return l

def get_cards_themes():
    l = []
    for root, dirs, files in os.walk('data/cards/'):
        if root == 'data/cards/' or root.find('.svn') >= 0:
            continue
        for i, file_name in enumerate(files):
            files[i] = os.path.join(root, file_name)
        theme_name = os.path.basename(root)
        l.append((os.path.join('share/minneo/cards', theme_name), files))
    return l

def get_gui_themes():
    l = []
    for root, dirs, files in os.walk('data/themes/'):
        if root == 'data/themes/' or root.find('images') >= 0 or root.find('.svn') >= 0:
            continue
        for i, file_name in enumerate(files[:]):
            if file_name == 'minneo.edj':
                files[i] = os.path.join(root, file_name)
            else:
                del files[i]
        theme_name = os.path.basename(root)
        l.append((os.path.join('share/minneo/themes', theme_name), files))
    return l

def main():
    setup(name         = 'minneo',
          version      = '1.0.3',
          description  = 'A classic Memory game for mobile devices',
          author       = 'Valery Febvre',
          author_email = 'vfebvre@easter-eggs.com',
          url          = 'http://code.google.com/p/minneo/',
          classifiers  = [
            'Development Status :: 2 - Beta',
            'Environment :: X11 Applications',
            'Intended Audience :: End Users/Phone UI',
            'License :: GNU General Public License (GPL)',
            'Operating System :: POSIX',
            'Programming Language :: Python',
            'Topic :: Desktop Environment',
            ],
          packages     = ['minneo'],
          scripts      = ['minneo/minneo'],
          data_files   = get_data_files(),
          )

if __name__ == '__main__':
    main()
