     __  __ _
    |  \/  (_)_ __  _ __   ___  ___  
    | |\/| | | '_ \| '_ \ / _ \/ _ \
    | |  | | | | | | | | |  __/ (_) |
    |_|  |_|_|_| |_|_| |_|\___|\___/

Description
===========
Minneo is a classic Memory game for mobile devices.

It's a game of memory and concentration.

https://gitlab.com/valos/minneo

It's written in Python/Elementary and licensed under
the GNU General Public License v3.


Features
========
- Single or two players
- Multi difficulty levels
- Multi themes
- High scores (in single player mode)


Authors
=======
Valéry Febvre <vfebvre@easter-eggs.com>


Requirements
============
- python-audio
- python-alsaaudio
- python-elementary


Notes
=====
- Minneo is tested on SHR unstable only.
  Normally, it should run on any system with a revision of
  python-elementary equal or greater to 40756.
- Minneo base home directory is $HOME/.config/minneo.
  This directory and related files are created after first run.


Todo
====
- More themes


Report Bugs
===========
If there is something wrong, please use to the Google code issues:
https://gitlab.com/valos/minneo/issues
Report a bug or look there for possible workarounds.


Credits
=======
- Suzanna Smith and Calum Benson for High Contrast icons.
- totya for sound Yeah found on freesound.org
- all authors of cliparts used for cards found on openclipart.org


Version History
===============

2012-04-17 - Version 1.0.3
--------------------------
- Release due to Elementary API changes

2010-11-17 - Version 1.0.2
--------------------------
- Bug fix release due to a Elementary API change in Toolbar widget

2010-05-27 - Version 1.0.1
--------------------------
- Major bugfixes release
- Fixed 3 bugs in high scores
- Added missing and essential rule in 'Two Player' mode:
  If you picked a correct pair, you get another turn
  Thanks to Vanous for report of this big mistake

2010-05-26 - Version 1.0.0
--------------------------
- First release
